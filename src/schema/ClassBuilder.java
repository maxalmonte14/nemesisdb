package schema;

import java.io.*;
import java.util.ArrayList;

public class ClassBuilder {

    private ArrayList<Field> fields;
    private String newClass;

    public ClassBuilder(ArrayList<Field> fields, String newClass) {
        this.fields = fields;
        this.newClass = newClass;
    }

    public void createClass() {

        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(String.format("src/%s.java",newClass)), "utf-8"));
            writer.write(String.format("public class %s {\n", newClass));

            for (Field field: fields) {
                writer.write(String.format("\tpublic %s %s;\n", field.getType(), field.getName()));
                writer.write(createSeter(field.getName(),field.getType()));
                writer.write("\n");
            }
            writer.write("}");
            writer.close();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String createSeter(String name, String type) {

        String finalName = "";
        for (int i = 0; i < name.length(); i++) {
            if (i == 0)
                finalName += String.valueOf(name.charAt(i)).toUpperCase();
            else
                finalName += String.valueOf(name.charAt(i)).toLowerCase();
        }

        String seter = String.format("\tpublic void set%s(%s %s) {\n\t\tthis.%s = %s;\n\t}\n", finalName, type, name, name, name);

        return seter;
    }

}
