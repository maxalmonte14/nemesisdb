package schema;

public class Field {

    private int length = 0;
    private String name;
    private String type;

    public Field(int length, String name, String type) {
        this.length = length;
        this.name = name;
        this.type = type;
    }

    public Field(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
